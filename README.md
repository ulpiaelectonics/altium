# Altium Components Library

## Passive components
This library contains symbols and footprints for passive
elements - resistors, capacitors and inductors.

### Resistors
### Capacitors
### Inductors

## Changelog
* 26.02.2018
    - Add 0603 resistor footprint
